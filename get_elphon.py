#!/usr/bin/env python
from xml.etree.ElementTree import *

iq=8
jr=[1,2,2,2,3,3,2,2]

if(len(jr)!=iq):
    print('error')
    exit()

def input_el_ph(iq,jr):
    g=[]
    for i in range(iq):
        g.append([])
        for j in range(jr[i]):
            tree=parse('elph.%d.%d.xml'%(i+1,j+1))
            obj=tree.getroot()
            knum=int(obj[1][0].text)
            nband=int(obj[1][1].text)
            g[i].append([])
            for k in range(knum):
                tmp=obj[1][k+2][0].text.split()
                nmode=int(obj[1][k+2][1].items()[1][1])/(nband*nband)
                rvec=[float(tp) for tp in tmp]
                tmp=obj[1][k+2][1].text.split('\n')
                tmp1=[tp.split(',') for tp in tmp]
                tmp=[complex(float(tp[0]),float(tp[1])) for tp in tmp1[1:-1]]
                tmp1=[[[tmp[l+m*nband+n*nband*nband] for l in range(nband)] 
                    for m in range(nband)] for n in range(nmode)]
                g[i][j].append(tmp1)

    return g


g=input_el_ph(iq,jr)
print(len(g))
